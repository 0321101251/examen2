from rest_framework import serializers
from .models import ToDo

## ALL
class ToDoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToDo
        fields = ['id', 'title', 'description', 'user', 'resolved']

## By ID
class ToDoIDSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToDo
        fields = ['id']

## By ID & Title
class ToDoIDTitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToDo
        fields = ['id', 'title']

## By ID & User
class ToDoIDUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToDo
        fields = ['id', 'user']