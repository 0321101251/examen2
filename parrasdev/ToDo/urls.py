# ToDo/urls.py
from django.urls import path
from .views import (
    index,
    ToDoListView,
    ToDoCreateView,
    ToDoCreateAPIView,
    ToDoUpdateView,
    ToDoUpdateAPIView,
    ToDoDeleteView,
    ToDoDeleteAPIView,
    ToDoListViewAPI,
    ToDoListIdView,
    ToDoListIdAPIView,
    ToDoListIdTitleView,
    ToDoListIdTitleAPIView,
    ToDoListIdUserView,
    ToDoListIdUserAPIView,
    ToDoResolvedIdTitleView,
    ToDoResolvedIdTitleAPIView,
    ToDoResolvedIdUserView,
    ToDoResolvedIdUserAPIView,
    ToDoUnresolvedIdTitleView,
    ToDoUnresolvedIdTitleAPIView,
    ToDoUnresolvedIdUserView,
    ToDoUnresolvedIdUserAPIView,
)

urlpatterns = [
    path('', index, name='home'),
    ## ALL
    path('todos/', ToDoListView.as_view(), name='todo_list'),
    path('api/todos/', ToDoListViewAPI.as_view(), name='todo_list_api'),
    ## CREATE
    path('todos/create/', ToDoCreateView.as_view(), name='todo_new'),
    path('api/todos/create/', ToDoCreateAPIView.as_view(), name='todo_create_api'),
    ## UPDATE
    path('todos/update/<int:pk>/', ToDoUpdateView.as_view(), name='todo_edit'),
    path('api/todos/update/<int:pk>/', ToDoUpdateAPIView.as_view(), name='todo_update_api'),
    ## DELETE
    path('todos/delete/<int:pk>/', ToDoDeleteView.as_view(), name='todo_delete'),
    path('api/todos/delete/<int:pk>/', ToDoDeleteAPIView.as_view(), name='todo_delete_api'),
    ## By ID
    path('todos/ids/', ToDoListIdView.as_view(), name='todo_list_ids'),
    path('api/todos/ids/', ToDoListIdAPIView.as_view(), name='todo_list_ids_api'),
    ## By ID & Title
    path('todos/ids_titles/', ToDoListIdTitleView.as_view(), name='todo_list_ids_titles'),
    path('api/todos/ids_titles/', ToDoListIdTitleAPIView.as_view(), name='todo_list_ids_titles_api'),
    ## By ID & User
    path('todos/ids_users/', ToDoListIdUserView.as_view(), name='todo_list_ids_users'),
    path('api/todos/ids_users/', ToDoListIdUserAPIView.as_view(), name='todo_list_ids_users_api'),
    ## RESOLVED by ID & TItle
    path('todos/resolved/ids_titles/', ToDoResolvedIdTitleView.as_view(), name='todo_res_ids_titles'),
    path('api/todos/resolved/ids_titles/', ToDoResolvedIdTitleAPIView.as_view(), name='todo_res_ids_titles_api'),
    ## RESOLVED by ID & User
    path('todos/resolved/ids_users/', ToDoResolvedIdUserView.as_view(), name='todo_res_ids_users'),
    path('api/todos/resolved/ids_users/', ToDoResolvedIdUserAPIView.as_view(), name='todo_res_ids_users_api'),
    ## UNRESOLVED by ID & Title
    path('todos/unresolved/ids_titles/', ToDoUnresolvedIdTitleView.as_view(), name='todo_unres_ids_titles'),
    path('api/todos/unresolved/ids_titles/', ToDoUnresolvedIdTitleAPIView.as_view(), name='todo_unres_ids_titles_api'),
    ## UNRESOLVED by ID & User
    path('todos/unresolved/ids_users/', ToDoUnresolvedIdUserView.as_view(), name='todo_unres_ids_users'),
    path('api/todos/unresolved/ids_users/', ToDoUnresolvedIdUserAPIView.as_view(), name='todo_unres_ids_users_api'),
]
