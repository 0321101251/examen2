from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from rest_framework import generics
from .models import ToDo
from .serializers import ToDoSerializer, ToDoIDSerializer, ToDoIDTitleSerializer, ToDoIDUserSerializer
from .forms import ToDoForm


def index(request):
    todos = ToDo.objects.all()
    return render(request, 'index.html', {'todos': todos})


class ToDoListView(ListView):
    model = ToDo
    template_name = 'todo_list.html'
    context_object_name = 'todos'

class ToDoListViewAPI(generics.ListAPIView):
    queryset = ToDo.objects.all()
    serializer_class = ToDoSerializer



## CREATE
class ToDoCreateView(CreateView):
    model = ToDo
    form_class = ToDoForm
    template_name = 'todo_form.html'
    success_url = reverse_lazy('todo_list')

class ToDoCreateAPIView(generics.CreateAPIView):
    queryset = ToDo.objects.all()
    serializer_class = ToDoSerializer



## UPDATE
class ToDoUpdateView(UpdateView):
    model = ToDo
    form_class = ToDoForm
    template_name = 'todo_form.html'
    success_url = reverse_lazy('todo_list')

class ToDoUpdateAPIView(generics.RetrieveUpdateAPIView):
    queryset = ToDo.objects.all()
    serializer_class = ToDoSerializer



## DELETE
class ToDoDeleteView(DeleteView):
    model = ToDo
    template_name = 'todo_confirm_delete.html'
    success_url = reverse_lazy('todo_list')

class ToDoDeleteAPIView(generics.RetrieveDestroyAPIView):
    queryset = ToDo.objects.all()
    serializer_class = ToDoSerializer



# By ID
class ToDoListIdView(ListView):
    template_name = 'todo_list_ids.html'
    context_object_name = 'todos'

    def get_queryset(self):
        return ToDo.objects.values('id')
    
class ToDoListIdAPIView(generics.ListAPIView):
    queryset = ToDo.objects.all()
    serializer_class = ToDoIDSerializer

    def get_queryset(self):
        return ToDo.objects.values('id')
    


#By ID & Title
class ToDoListIdTitleView(ListView):
    template_name = 'todo_list_ids_titles.html'
    context_object_name = 'todos'

    def get_queryset(self):
        return ToDo.objects.values('id', 'title')

class ToDoListIdTitleAPIView(generics.ListAPIView):
    queryset = ToDo.objects.all()
    serializer_class = ToDoIDTitleSerializer

    def get_queryset(self):
        return ToDo.objects.values('id', 'title')



## By ID & User
class ToDoListIdUserView(ListView):
    template_name = 'todo_list_ids_users.html'
    context_object_name = 'todos'

    def get_queryset(self):
        return ToDo.objects.select_related('user').only('id', 'user__id')

class ToDoListIdUserAPIView(generics.ListAPIView):
    queryset = ToDo.objects.all()
    serializer_class = ToDoIDUserSerializer

    def get_queryset(self):
        return ToDo.objects.select_related('user').only('id', 'user__id')



# RESOLVED by ID & Title
class ToDoResolvedIdTitleView(ListView):
    template_name = 'todo_res_ids_titles.html'
    context_object_name = 'todos'

    def get_queryset(self):
        return ToDo.objects.filter(resolved=True).values('id', 'title')

class ToDoResolvedIdTitleAPIView(generics.ListAPIView):
    queryset = ToDo.objects.filter(resolved=True)
    serializer_class = ToDoIDTitleSerializer

    def get_queryset(self):
        return ToDo.objects.filter(resolved=True).values('id', 'title')



# RESOLVED by ID & User
class ToDoResolvedIdUserView(ListView):
    template_name = 'todo_res_ids_users.html'
    context_object_name = 'todos'

    def get_queryset(self):
        return ToDo.objects.filter(resolved=True).select_related('user').only('id', 'user__id')
    
class ToDoResolvedIdUserAPIView(generics.ListAPIView):
    queryset = ToDo.objects.filter(resolved=True)
    serializer_class = ToDoIDUserSerializer

    def get_queryset(self):
        return ToDo.objects.filter(resolved=True).select_related('user').only('id', 'user__id')
    


# UNRESOLVED by ID & Title
class ToDoUnresolvedIdTitleView(ListView):
    template_name = 'todo_unres_ids_titles.html'
    context_object_name = 'todos'

    def get_queryset(self):
        return ToDo.objects.filter(resolved=False).values('id', 'title')

class ToDoUnresolvedIdTitleAPIView(generics.ListAPIView):
    queryset = ToDo.objects.filter(resolved=False)
    serializer_class = ToDoIDTitleSerializer

    def get_queryset(self):
        return ToDo.objects.filter(resolved=False).values('id', 'title')



## UNRESOLVED by ID & User
class ToDoUnresolvedIdUserView(ListView):
    template_name = 'todo_unres_ids_users.html'
    context_object_name = 'todos'

    def get_queryset(self):
        return ToDo.objects.filter(resolved=False).select_related('user').only('id', 'user__id')

class ToDoUnresolvedIdUserAPIView(generics.ListAPIView):
    queryset = ToDo.objects.filter(resolved=False)
    serializer_class = ToDoIDUserSerializer

    def get_queryset(self):
        return ToDo.objects.filter(resolved=False).select_related('user').only('id', 'user__id')
